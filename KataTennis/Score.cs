﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KataTennis
{
    class Score
    {
        public static string[] C_POSSIBLE_SCORES { get; } = { "0", "15", "30", "40" };
        public int Value { get; private set; } = 0;

        public Score()
        {

        }
        public void IncrementValueByOne()
        {
            Value++;
        }
        public void Reset()
        {
            Value = 0;
        }
        override public string ToString()
        {
            string score = "";

            if (Value > C_POSSIBLE_SCORES.Length - 1)
            {
                score = C_POSSIBLE_SCORES[C_POSSIBLE_SCORES.Length - 1];
            }
            else
            {
                score = C_POSSIBLE_SCORES[Value];
            }

            return score;
        }
    }
}
