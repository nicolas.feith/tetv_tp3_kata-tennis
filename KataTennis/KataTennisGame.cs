﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KataTennis
{
    public class KataTennisGame
    {
        private Score player1Score;
        private Score player2Score;
        private uint advantage = 0;

        public KataTennisGame()
        {
            player1Score = new Score();
            player2Score = new Score();
        }
        public string GameScore()
        {
            CheckGameState();
            string player1ScoreAsAString = player1Score.ToString();
            string player2ScoreAsAString = player2Score.ToString();
            if (advantage == 1)
            {
                player1ScoreAsAString += "A";
            }
            else if (advantage == 2)
            {
                player2ScoreAsAString += "A";
            }

            return player1ScoreAsAString + " - " + player2ScoreAsAString;
        }
        public void Player1Score()
        {
            player1Score.IncrementValueByOne();
        }
        public void Player2Score()
        {
            player2Score.IncrementValueByOne();
        }
        private void CheckGameState()
        {
            if ( (player1Score.Value > Score.C_POSSIBLE_SCORES.Length - 1) 
                || (player2Score.Value > Score.C_POSSIBLE_SCORES.Length - 1))
            {
                WhoHasAdvantage();
            }
        }
        private void WhoHasAdvantage()
        {
            if (player1Score.Value - player2Score.Value == 1)
            {
                advantage = 1;
            }
            else if (player1Score.Value - player2Score.Value == -1)
            {
                advantage = 2;
            }
            else if (player1Score.Value - player2Score.Value == 0)
            {
                advantage = 0;
            }
            else
            {
                WinGame();
            }
        }
        private void WinGame()
        {
            player1Score.Reset();
            player2Score.Reset();
            advantage = 0;
        }
    }
}
