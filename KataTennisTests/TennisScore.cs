using System;
using Xunit;
using KataTennis;

namespace KataTennisTests
{
    public class TennisScore
    {
        // Note: Love is equivalent of zero in tennis.
        [Fact]
        public void LoveAll()
        {
            var kataTennisGame = new KataTennisGame();
            Assert.Equal("0 - 0", kataTennisGame.GameScore());
        }

        [Fact]
        public void FifteenLove()
        {
            var kataTennisGame = new KataTennisGame();
            kataTennisGame.Player2Score();
            Assert.Equal("0 - 15", kataTennisGame.GameScore());
        }

        [Fact]
        public void AdvantageFourteen()
        {
            var kataTennisGame = new KataTennisGame();
            kataTennisGame.Player1Score();
            kataTennisGame.Player1Score();
            kataTennisGame.Player1Score();
            kataTennisGame.Player2Score();
            kataTennisGame.Player2Score();
            kataTennisGame.Player2Score();
            kataTennisGame.Player2Score();

            Assert.Equal("40 - 40A", kataTennisGame.GameScore());
        }

        [Fact]
        public void BackToDeuceAfterAdvantage()
        {
            var kataTennisGame = new KataTennisGame();
            kataTennisGame.Player1Score();
            kataTennisGame.Player1Score();
            kataTennisGame.Player1Score();
            kataTennisGame.Player2Score();
            kataTennisGame.Player2Score();
            kataTennisGame.Player2Score();
            kataTennisGame.Player2Score();

            kataTennisGame.Player1Score();

            Assert.Equal("40 - 40", kataTennisGame.GameScore());
        }

        [Fact]
        public void WinWithoutAdvantage()
        {
            var kataTennisGame = new KataTennisGame();
            kataTennisGame.Player1Score();
            kataTennisGame.Player1Score();
            kataTennisGame.Player1Score();
            kataTennisGame.Player1Score();

            Assert.Equal("0 - 0", kataTennisGame.GameScore());
        }
        [Fact]
        public void WinAfterAdvantage()
        {
            var kataTennisGame = new KataTennisGame();
            kataTennisGame.Player1Score();
            kataTennisGame.Player1Score();
            kataTennisGame.Player1Score();

            kataTennisGame.Player2Score();
            kataTennisGame.Player2Score();
            kataTennisGame.Player2Score();
            kataTennisGame.Player2Score();

            kataTennisGame.Player2Score();

            Assert.Equal("0 - 0", kataTennisGame.GameScore());
        }
    }
}
